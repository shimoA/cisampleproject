import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class SampleTest {
    private Sample testTarget;

    @BeforeEach
    public void setUp() {
        testTarget = new Sample();
    }

    @Test
    public void test() {
        String result = testTarget.execute();
        assertThat(result, is("ok"));
    }
}
